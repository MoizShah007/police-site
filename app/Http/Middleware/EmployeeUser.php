<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
class EmployeeUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
             if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'employee')
    {   return $next($request);
             
        }else{
             Sentinel::logout();
        return redirect('/employee-login');
            
        }
        //return $next($request);
    }
}
