<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
class PoliceUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'police')
    {   return $next($request);
             
        }else{
             Sentinel::logout();
        return redirect('/police-login');
            
        }
        //return $next($request);
    }
}
