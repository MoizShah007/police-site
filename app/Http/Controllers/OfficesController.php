<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offices;
use App\Cases;
use Session;
class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('admin');
    }


    public function index()
    {
        $office = Offices::where('status',1)->get();
        return view('admin.office.index',compact('office'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     
         public function officeCases($id){
        
    //$cases = Cases::where('office_id',$id)->get();
        $cases = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.review','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.office_id',$id)->get(); 
        
        $cases_5 = json_decode(json_encode($cases),true);
        $cases_5 =array_map('array_filter',$cases_5);
        $cases_5=array_map(function ($cases_5){
            if(!in_array(null,$cases_5))
                return $cases_5;
        },$cases_5);
        $cases_5=array_filter($cases_5);
        
      
        
    return view('admin.office.cases',compact('cases_5'));
    }
    
    
    public function caseTimeLine($id){
        
        $case = Cases::where('id',$id)->first();
        return view('admin.office.case-timeline',compact('case'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'address' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',
            
        ]);
        
           
           $office_status =   Offices::where('office_status' ,1)->where('status' ,1)->first();
        
        
        if(count($office_status)>0){
                if(count($request->unit_no) > 0){
            $office = new  Offices;
        $office->rand_id = 'ok';
        $office->name = $request->name;
        $office->location = $request->location;
        $office->address = $request->address;
        $office->street = $request->street;
        $office->unit_no = $request->unit_no;
        $office->city = $request->city;
        $office->state = $request->state;
        $office->country = $request->country;
        $office->zip = $request->zip;
        $office->save(); 
        }else{
            $office = new  Offices;
        $office->rand_id = 'ok';
        $office->name = $request->name;
        $office->location = $request->location;
        $office->address = $request->address;
        $office->street = $request->street;
        $office->city = $request->city;
        $office->state = $request->state;
        $office->country = $request->country;
        $office->zip = $request->zip;
        $office->save();
        }
        }else{
                    if(count($request->unit_no) > 0){
            $office = new  Offices;
        $office->rand_id = 'ok';
        $office->name = $request->name;
        $office->location = $request->location;
        $office->address = $request->address;
        $office->street = $request->street;
        $office->unit_no = $request->unit_no;
        $office->city = $request->city;
        $office->state = $request->state;
        $office->country = $request->country;
        $office->zip = $request->zip;
        $office->office_status = $request->office_status;
        $office->save(); 
        }else{
            $office = new  Offices;
        $office->rand_id = 'ok';
        $office->name = $request->name;
        $office->location = $request->location;
        $office->address = $request->address;
        $office->street = $request->street;
        $office->city = $request->city;
        $office->state = $request->state;
        $office->country = $request->country;
        $office->zip = $request->zip;
        $office->office_status = $request->office_status;
        $office->save();
        }
        }
        

        
        $msg = 'Office is created';
        Session::flash('success' , $msg);
        return redirect()->back();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $office = Offices::where('id',$id)->first();
        return view('admin.office.edit',compact('office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'address' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',
            
        ]);
         
       
        if(count($request->unit_no) > 0){
           Offices::where('id', $id)->update(['name' => $request->name,
        'location' => $request->location,'address' => $request->address,'address' => $request->address,'street' => $request->street,'unit_no' => $request->unit_no,'city' => $request->city,'state' => $request->state,'country' => $request->country,'zip' => $request->zip]);   
        }
        else{
              Offices::where('id', $id)->update(['name' => $request->name,
        'location' => $request->location,'address' => $request->address,'address' => $request->address,'street' => $request->street,'city' => $request->city,'state' => $request->state,'country' => $request->country,'zip' => $request->zip]);
        }
       $msg = "Office is  is updated";
       Session::flash('success', $msg);
      return redirect('office/');
    }


    public function deleteOffices(Request $request){
      
        $request_data = $request->favorite;
        foreach ($request_data as $key ) {
             Offices::where('id', $key)->update(['status' => 0]);
        }
       $msg = "Offices is deleted";
       Session::flash('success', $msg);
       return redirect('office/');

    }
        public function deleteOffice($id){
      
         Offices::where('id', $id)->update(['status' => 0]);
       $msg = "Offices is deleted";
       Session::flash('success', $msg);
       return redirect('office/');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
