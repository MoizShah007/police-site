<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cases;
use Sentinel;
use Session;
use App\PoliceFiles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
class PoliceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //        $cases_1 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //        ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //           
        //        ->where('cases.status',1)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        //        $cases_2 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //        ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //           
        //        ->where('cases.status',2)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        //          $cases_3 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //        ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //           
        //        ->where('cases.status',3)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        //
        //         $cases_4 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //        ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //           
        //        ->where('cases.status',4)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        //          $cases_5 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //        ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //           
        //        ->where('cases.status',5)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        
        $cases1 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',1)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        $cases_1 = json_decode(json_encode($cases1),true);
        $cases_1 =array_map('array_filter',$cases_1);
        $cases_1=array_map(function ($cases_1){
            if(!in_array(null,$cases_1))
                return $cases_1;
        },$cases_1);
        $cases_1=array_filter($cases_1);

        $cases2 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',2)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        $cases_2 = json_decode(json_encode($cases2),true);
        $cases_2 =array_map('array_filter',$cases_2);
        $cases_2=array_map(function ($cases_2){
            if(!in_array(null,$cases_2))
                return $cases_2;
        },$cases_2);
        $cases_2=array_filter($cases_2);

        $cases3= Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',3)->where('cases.police_id',Sentinel::getUser()->id)->get(); 
        $cases_3 = json_decode(json_encode($cases3),true);
        $cases_3 =array_map('array_filter',$cases_3);
        $cases_3=array_map(function ($cases_3){
            if(!in_array(null,$cases_3))
                return $cases_3;
        },$cases_3);
        $cases_3=array_filter($cases_3);

        $cases4 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.review','cases.service_name','cases.service_name','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',4)->where('cases.police_id',Sentinel::getUser()->id)->get(); 

        $cases_4 = json_decode(json_encode($cases4),true);
        $cases_4 =array_map('array_filter',$cases_4);
        $cases_4=array_map(function ($cases_4){
            if(!in_array(null,$cases_4))
                return $cases_4;
        },$cases_4);
        $cases_4=array_filter($cases_4);
        
        $cases5 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.review','cases.service_name','cases.service_name','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',5)->where('cases.police_id',Sentinel::getUser()->id)->get(); 

        $cases_5 = json_decode(json_encode($cases5),true);
        $cases_5 =array_map('array_filter',$cases_5);
        $cases_5=array_map(function ($cases_5){
            if(!in_array(null,$cases_5))
                return $cases_5;
        },$cases_5);
        $cases_5=array_filter($cases_5);
        return view('police.case',compact('cases_1','cases_2','cases_3','cases_4','cases_5'));
        //return view('police.case');
    }


   public function caseReivew($id){

         $cases =  Cases::where('id',$id)->first();
      
        if($cases->service_name == "Finger Prints Form"){
    $case = Cases::select('cases.id','finger_print.id as finger_id','finger_print.rand_id as finger_rand_id','declare_criminal_record.id as declare_id','criminal_record_verify.id as verify_id','contact_no','finger_print.last_name as finger_last_name','criminal_record_verify.last_name as verify_last_name','declare_criminal_record.last_name as declare_last_name','finger_print.given_name as finger_given_name','criminal_record_verify.given_name as verify_given_name','declare_criminal_record.given_name as declare_given_name','last_name_birth','former_name','birth_place','finger_print.date_of_birth as finger_date_of_birth','criminal_record_verify.date_of_birth as verify_date_of_birth','declare_criminal_record.date_of_birth as declare_date_of_birth','phone_number','gender','reason_to_request','contact_name','organization_request','organization_address','cpic_investegation','police_info','other','auth_to','auth_located','auth_location','auth_signature','finger_print.date_of_sign as finger_date_of_sign','criminal_record_verify.date_of_sign as verify_date_of_sign','declare_criminal_record.date_of_sign as declare_date_of_sign','auth_city','auth_province','auth_witness_agent','auth_identification','auth_witness_agent_signature','auth_id','passport','idcard','sex','current_address','city','province','postal_code','criminal_record_verify.sign_of_applicant as verify_sign_of_applicant','declare_criminal_record.sign_of_applicant as declare_sign_of_applicant','verify_by','sign_of_police')
                ->join("finger_print","finger_print.rand_id", "=", "cases.service_id")
                ->leftJoin("criminal_record_verify","criminal_record_verify.finger_print_id", "=", "finger_print.id")
                ->leftJoin("declare_criminal_record","declare_criminal_record.finger_print_id", "=", "finger_print.id")
                    ->where('cases.id',$id)->first(); 
                    if($cases->status == 1 ){
                      
                          Cases::where('id', $id)->update(['status'=> 2,'open_date' =>date('Y-m-d')]);
                    }
   
            return view('police.forms.fingerprint',compact('case'));
            
        }elseif($cases->service_name == "Criminal Record Verification"){
            $case = Cases::join("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
                    ->where('cases.id',$id)->first(); 
            if($cases->status == 1 ){
                           Cases::where('id', $id)->update(['status'=> 2,'open_date' =>date('Y-m-d')]);
                    }
            return view('police.forms.criminalrecordverify',compact('case'));
            
        }elseif($cases->service_name == "Declaration Criminal Record"){
            $case = Cases::join("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
                    ->where('cases.id',$id)->first(); 
        
            if($cases->status == 1 ){
                         Cases::where('id', $id)->update(['status'=> 2,'open_date' =>date('Y-m-d')]);
                    }
            return view('police.forms.declarationcriminalrecord',compact('case'));
        }



    }


    public function caseStatus(Request $request, $id){

        
        
        $this->validate($request, [
            'status' => 'required',
            'finger_date_of_birth' => 'required',
            'finger_last_name'=> 'required'

        ]);
       
        $status = $request->status;
        $finger_date_of_birth = $request->finger_date_of_birth;
        $finger_last_name = $request->finger_last_name;
        // if(count($request->review) > 0){
        //     Cases::where('id', $id)->update(['status' => $status,'review'=>$request->review,'updated_at'=>date('Y-m-d G:i:s')]);
        // }else{
        //     Cases::where('id', $id)->update(['status' => $status,'updated_at'=>date('Y-m-d G:i:s')]);
        // }
        if($status == 3){
             Cases::where('id', $id)->update(['status' => $status,'updated_at'=>date('Y-m-d G:i:s')]);
            return view('police.status.incomplete',compact('finger_date_of_birth','finger_last_name'));
        }elseif($status == 4){
            Cases::where('id', $id)->update(['status' => $status,'closed_date'=>date('Y-m-d')]);
            return view('police.status.confirmation',compact('finger_date_of_birth','finger_last_name'));
            
        }elseif($status == 5 ){
               Cases::where('id', $id)->update(['status' => $status,'closed_date'=>date('Y-m-d')]);
            return view('police.status.negative',compact('finger_date_of_birth','finger_last_name'));
        }
        
        
        
//        
//        if(count($request->review) > 0){
//            Cases::where('id', $id)->update(['status' => $status,'review'=>$request->review,'updated_at'=>date('Y-m-d G:i:s')]);
//        }else{
//            Cases::where('id', $id)->update(['status' => $status,'updated_at'=>date('Y-m-d G:i:s')]);
//        }
//
//        $msg = "Status Updated";
//        Session::flash('success', $msg);
//        return redirect('/polices');


    }

    public function uploadFiles(Request $request,$id){
        $path = $request->file('file')->store('avatars');
        $path = Storage::putFile('avatars', $request->file('file'));

        $policeFiles = new PoliceFiles;
        $policeFiles->rand_id = 'ok';
        $policeFiles->file = $path;
        $policeFiles->case_id =$id;
        $policeFiles->save();

        $msg = "File Uploaded";
        Session::flash('success', $msg);
        return redirect('/polices');

    }

    public function fileUpload($id){

        return view('police.fileupload',compact('id'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
