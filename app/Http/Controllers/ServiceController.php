<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Session;
use App\Offices;
use App\ServiceOffices;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $service = Services::where('status',1)->get();
        $office = Offices::where('status',1)->get();
        return view('admin.service.index',compact('service','office'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'service_forms' => 'required',
            'image' => 'required',
            'office_id' => 'required'
        ]);
        if(Input::file('image')){
            //$path = $request->file('image')->store('avatars');
            // $path = Storage::putFile('avatars', $request->file('image'));
            //          return $path;

            $image = Input::file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('application-file/img/' . $filename);
            Image::make($image->getRealPath())->resize(200, 300)->save($path);

            $service = new  Services;
            $service->rand_id = 'ok';
            $service->name = $request->name;
            $service->service_forms = $request->service_forms;
            $service->image = $filename;
            $service->save();


            foreach($request->office_id as $value){
                $office_service = new ServiceOffices;
                $office_service->service_id =  $service->id;
                $office_service->rand_id='ok';
                $office_service->office_id= $value;
                $office_service->save();
            }
            $msg = 'Service is created';
            Session::flash('success' , $msg);
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Services::where('id',$id)->first();
        // $office = Offices::join("service_offices", "service_offices.office_id", "=", "offices.id")
        // ->where('offices.status',1)
        // ->get();
         $office = Offices::where('status',1)->get();
            //Offices::where('users.id',$id)->first();
        
        return view('admin.service.edit',compact('service','office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'service_forms' => 'required',
            'office_id' => 'required'

        ]);

        if(Input::file('image')){

            $image = Input::file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('application-file/img/' . $filename);
            Image::make($image->getRealPath())->resize(200, 300)->save($path);

            Services::where('id', $id)->update(['name' => $request->name,'service_forms' => $request->service_forms,'image' =>$filename]);
            if(count($request->office_id)>0){
                ServiceOffices::where('service_id',$id)->delete();
                foreach($request->office_id as $value){

                    $office_service = new ServiceOffices;
                    $office_service->service_id =  $id;
                    $office_service->rand_id='ok';
                    $office_service->office_id= $value;
                    $office_service->save();
                }
            }
            $msg = "Service is  is updated";
            Session::flash('success', $msg);
            return redirect('/service');

        }else{
            Services::where('id', $id)->update(['name' => $request->name,
                                                'service_forms' => $request->service_forms]);

            if(count($request->office_id)>0){
                ServiceOffices::where('service_id',$id)->delete();
                foreach($request->office_id as $value){

                    $office_service = new ServiceOffices;
                    $office_service->service_id =  $id;
                    $office_service->rand_id='ok';
                    $office_service->office_id= $value;
                    $office_service->save();
                }
            }

            $msg = "Services is  is updated";
            Session::flash('success', $msg);
            return redirect('/service');
        }


    }
 public function deleteServices(Request $request){
      
        $request_data = $request->favorite;
        foreach ($request_data as $key ) {
             Services::where('id', $key)->update(['status' => 0]);
        }
       
       $msg = "Services is deleted";
       Session::flash('success', $msg);
       return redirect('service/');

    }
    public function deleteService($id){
      
         Services::where('id', $id)->update(['status' => 0]);
       $msg = "Services is deleted";
       Session::flash('success', $msg);
       return redirect('service/');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
