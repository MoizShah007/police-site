<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use Sentinel;
use Session, Activation, Validator, Reminder;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\EmailController;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function adminLoginIn()
    {

        try {
            $input = Input::all();
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $msg = "Data is not validated";

                Session::flash('error', $msg);
                return redirect()->back();


            }

            $remember = (bool)Input::get('remember', false);

            $credentials = [
                'email' => $input['email'],
                'password' => $input['password'],
            ];

            if (Sentinel::authenticate($credentials, $remember)) {



                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin'){
                    return redirect(url('/admin-dashboard'));

                }

                Sentinel::logout();
                $msg = "Invalid email or password.";
                Session::flash('error', $msg);
                return redirect()->back();


            }

        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {


            $msg = "Your Account is not acitavied please check your email.";
            Session::flash('error', $msg);
            return redirect()->back();


        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {


            $delay = $e->getDelay();
            $msg = "Your account is blocked for {$delay} second(s).";

            Session::flash('error', $msg);
            return redirect()->back();


        }

        $msg = "Invalid email or password.";
        Session::flash('error', $msg);
        return redirect()->back();


    }

    public function empolyeeLogin(){
        try {
            $input = Input::all();
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $msg = "Data is not validated";

                Session::flash('error', $msg);
                return redirect()->back();
            }

            $remember = (bool)Input::get('remember', false);

            $credentials = [
                'email' => $input['email'],
                'password' => $input['password'],
            ];

            if (Sentinel::authenticate($credentials, $remember)) {
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'employee'){
                    return redirect(url('/select-office'));
                }

                Sentinel::logout();
                $msg = "Invalid email or password.";
                Session::flash('error', $msg);
                return redirect()->back();


            }

        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {


            $msg = "Your Account is not acitavied please check your email.";
            Session::flash('error', $msg);
            return redirect()->back();


        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {


            $delay = $e->getDelay();
            $msg = "Your account is blocked for {$delay} second(s).";

            Session::flash('error', $msg);
            return redirect()->back();


        }

        $msg = "Invalid email or password.";
        Session::flash('error', $msg);
        return redirect()->back();


    }
      public function customerLogin(){
        try {
            $input = Input::all();
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $msg = "Data is not validated";

                Session::flash('error', $msg);
                return redirect()->back();
            }

            $remember = (bool)Input::get('remember', false);

            $credentials = [
                'email' => $input['email'],
                'password' => $input['password'],
            ];

            if (Sentinel::authenticate($credentials, $remember)) {
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'user'){
                    return redirect(url('/customer-dashboard'));
                }

                Sentinel::logout();
                $msg = "Invalid email or password.";
                Session::flash('error', $msg);
                return redirect()->back();


            }

        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {


            $msg = "Your Account is not acitavied please check your email.";
            Session::flash('error', $msg);
            return redirect()->back();


        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {


            $delay = $e->getDelay();
            $msg = "Your account is blocked for {$delay} second(s).";

            Session::flash('error', $msg);
            return redirect()->back();


        }

        $msg = "Invalid email or password.";
        Session::flash('error', $msg);
        return redirect()->back();


    }
     public function policeLogin(){
        
        try {
            $input = Input::all();
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $msg = "Data is not validated";

                Session::flash('error', $msg);
                return redirect()->back();
            }

            $remember = (bool)Input::get('remember', false);

            $credentials = [
                'email' => $input['email'],
                'password' => $input['password'],
            ];

            if (Sentinel::authenticate($credentials, $remember)) {
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'police'){
                   return redirect(url('/police-dashboard'));
                }

                Sentinel::logout();
                $msg = "Invalid email or password.";
                Session::flash('error', $msg);
                return redirect()->back();


            }

        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {


            $msg = "Your Account is not acitavied please check your email.";
            Session::flash('error', $msg);
            return redirect()->back();


        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {


            $delay = $e->getDelay();
            $msg = "Your account is blocked for {$delay} second(s).";

            Session::flash('error', $msg);
            return redirect()->back();


        }

        $msg = "Invalid email or password.";
        Session::flash('error', $msg);
        return redirect()->back();


    }
   public function adminLogout()
    {
        Sentinel::logout();

        if (isset($msg)) {

            Session::flash('success', $msg);

        } else {
            Session::flash('success', "You are Logged out. ");
        }
        return redirect(url('/admin-login'));

    }
    public function employeeLogout()
    {
        Sentinel::logout();

        if (isset($msg)) {

            Session::flash('success', $msg);

        } else {
            Session::flash('success', "You are Logged out. ");
        }
        return redirect(url('/employee-login'));

    }
        public function customerLogout()
    {
        Sentinel::logout();

        if (isset($msg)) {

            Session::flash('success', $msg);

        } else {
            Session::flash('success', "You are Logged out. ");
        }
        return redirect(url('/organization-login'));

    }
    public function policeLogout()
    {
        Sentinel::logout();

        if (isset($msg)) {

            Session::flash('success', $msg);

        } else {
            Session::flash('success', "You are Logged out. ");
        }
        return redirect(url('/police-login'));

    }

}
