<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceOffices;
use Sentinel;
use App\Offices;
use App\Services;
use App\Employee;
use App\Customer;
use App\FingerPrint;
use App\FringerPrintAddress;
use App\Cases;
use Session;
use App\Police;
use Image;
use App\FingerPrintImage;
use App\CriminalRecordVerify;
use App\DelcareCriminalRecord;
use App\DelcareCriminalRecordOffence;
use App\CriminalRecordVerifyOffence;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
   public function dashboard(){

        return view('customer.dashboard');

    }
 
    public function cases(){
      
        $cases1 = Cases::select('cases.id','finger_print.last_name as finger_print_name','finger_print.rand_id as rand_ids','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',1)->where('case_owner',Sentinel::getUser()->id)->get(); 
        $cases_1 = json_decode(json_encode($cases1),true);
        $cases_1 =array_map('array_filter',$cases_1);
        $cases_1=array_map(function ($cases_1){
            if(!in_array(null,$cases_1))
                return $cases_1;
        },$cases_1);
        $cases_1=array_filter($cases_1);

        $cases2 = Cases::select('cases.id','finger_print.last_name as finger_print_name','finger_print.rand_id as rand_ids','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',2)->where('case_owner',Sentinel::getUser()->id)->get(); 
        $cases_2 = json_decode(json_encode($cases2),true);
        $cases_2 =array_map('array_filter',$cases_2);
        $cases_2=array_map(function ($cases_2){
            if(!in_array(null,$cases_2))
                return $cases_2;
        },$cases_2);
        $cases_2=array_filter($cases_2);

        $cases3= Cases::select('cases.id','finger_print.last_name as finger_print_name','finger_print.rand_id as rand_ids','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',3)->where('case_owner',Sentinel::getUser()->id)->get(); 
        $cases_3 = json_decode(json_encode($cases3),true);
        $cases_3 =array_map('array_filter',$cases_3);
        $cases_3=array_map(function ($cases_3){
            if(!in_array(null,$cases_3))
                return $cases_3;
        },$cases_3);
        $cases_3=array_filter($cases_3);

        $cases4 = Cases::select('cases.id','finger_print.last_name as finger_print_name','finger_print.rand_id as rand_ids','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.review','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',4)->where('case_owner',Sentinel::getUser()->id)->get(); 

        $cases_4 = json_decode(json_encode($cases4),true);
        $cases_4 =array_map('array_filter',$cases_4);
        $cases_4=array_map(function ($cases_4){
            if(!in_array(null,$cases_4))
                return $cases_4;
        },$cases_4);
        $cases_4=array_filter($cases_4);


        $cases5 = Cases::select('cases.id','finger_print.last_name as finger_print_name','finger_print.rand_id as rand_ids','finger_print.phone_number','finger_print.given_name as finger_given_name','finger_print.gender','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name','cases.review','cases.service_name')
            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
            ->where('cases.status',5)->where('case_owner',Sentinel::getUser()->id)->get(); 

        $cases_5 = json_decode(json_encode($cases5),true);
        $cases_5 =array_map('array_filter',$cases_5);
        $cases_5=array_map(function ($cases_5){
            if(!in_array(null,$cases_5))
                return $cases_5;
        },$cases_5);
        $cases_5=array_filter($cases_5);

            
        


        //        echo '<pre>';
        //            print_r($data);
        //        
        //echo $cases_1;
        //        exit;
        //          $cases_1 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name')
        //            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
        //            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
        //            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
        //            ->where('cases.status',1)->where('office_id',$id)->get();
        //          $cases_2 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name')
        //            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
        //            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
        //            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
        //            ->where('cases.status',2)->where('office_id',$id)->get();
        //          $cases_3 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name')
        //            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
        //            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
        //            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
        //            ->where('cases.status',3)->where('office_id',$id)->get();
        //          $cases_4 = Cases::select('cases.id','finger_print.last_name as finger_print_name','criminal_record_verify.last_name as criminal_record_verify_name','declare_criminal_record.last_name as declare_criminal_record_name')
        //            ->leftJoin("finger_print","finger_print.rand_id", "=", "cases.service_id")
        //            ->leftJoin("criminal_record_verify","criminal_record_verify.rand_id", "=", "cases.service_id")
        //            ->leftJoin("declare_criminal_record","declare_criminal_record.rand_id", "=", "cases.service_id")
        //            ->where('cases.status',4)->where('office_id',$id)->get();
        //        $cases_1 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //            ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //
        //            ->where('cases.status',1)->where('office_id',$id)->get(); 
        //        
        //        $cases_2 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //            ->join("finger_print","finger_print.id", "=", "cas;es.service_id")
        //
        //            ->where('cases.status',2)->where('office_id',$id)->get();
        //        $cases_3 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //            ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //
        //            ->where('cases.status',3)->where('office_id',$id)->get();
        //        $cases_4 = Cases::select('cases.id','finger_print.given_name','finger_print.phone_number','cases.created_at')
        //            ->join("finger_print","finger_print.id", "=", "cases.service_id")
        //
        //            ->where('cases.status',4)->where('office_id',$id)->get();
        $role = Sentinel::findRoleById(4);
        $police = $role->users()->with('roles')->where('status',1)->get();

        return view('customer.index',compact('cases','police','cases_1','cases_2','cases_3','cases_4','cases_5'));


    }
       public function createCase(){
$service = Services::where("status",1)->get();
        // $service = ServiceOffices::
        // join("services","services.id", "=", "service_offices.service_id")
        //     ->where("service_offices.office_id",$id)
        //      ->where("services.status",1)
        //     ->get();
        return view('customer.cases.create',compact('service'));

    }
      public function serviceForm($id){

        if($id == 'Finger Prints Form'){
            return view('customer.forms.fingerprint');
        }elseif($id == 'Criminal Record Verification'){
            return view('customer.forms.criminalrecordverify');
        }elseif($id == 'Declaration Criminal Record'){
            return view('customer.forms.declarationcriminalrecord');
        }
    }
    public function fingerPrintService(Request $request){

   
        $this->validate($request, [
           
            'last_name' => 'required',
            'given_name' => 'required',
            'last_name_birth' => 'required',
            'former_name' => 'required',
            'birth_place' => 'required',
            'date_of_birth' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'house_no' => 'required',
            'apartment_no' => 'required',
            'address' => 'required',
            // 'passport'=> 'required',
            // 'idcard'=> 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'province' => 'required',
            'country' => 'required',
            'reason_to_request' => 'required',
            'contact_name' => 'required',
            'organization_request' => 'required',
            'organization_address' => 'required',
            'contact_no' => 'required',
            'auth_to' => 'required',
            'auth_located' => 'required',
            'auth_location' => 'required',
            //'auth_signature' => 'required',
            'date_of_sign' => 'required',
            'auth_city' => 'required',
            'auth_province' => 'required',
            'auth_witness_agent' => 'required',
            'auth_identification' => 'required',
            //'auth_witness_agent_signature' => 'required',
            'auth_id' => 'required',
        ]);
        $data = $request->all();
        if(Input::file('passport')){
        $image = Input::file('passport');
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('/application-file/img/' . $filename);
        Image::make($image->getRealPath())->resize(200, 300)->save($path);
        $data['passport'] = $filename;
            // $passport = $request->file('passport')->store('avatars');
            // $passport = Storage::putFile('avatars', $request->file('passport'));
            // $data['passport'] = $passport;
        }
        if(Input::file('idcard')){
        $image = Input::file('idcard');
      $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('/application-file/img/' . $filename);
        Image::make($image->getRealPath())->resize(200, 300)->save($path);
            // $idcard = $request->file('idcard')->store('avatars');
            // $idcard = Storage::putFile('avatars', $request->file('idcard'));
            $data['idcard'] = $filename;
        }
          $rand_id = uniqid();
        $data['rand_id'] = $rand_id;
        $data['employee_id'] = Sentinel::getUser()->id;
        $finger_print_service = FingerPrint::create($data);
   if(!empty($_POST['namafoto'])){
            $image= explode(",",$request->namafoto);
              $name= explode(",",$request->image_name);
     
            
            for($i = 0; $i < count($image); $i++){
            
                $snap_image =  $image[$i];
                $snap_name =  $name[$i];
                
            $binary_data = base64_decode($snap_image);
            $namafoto = uniqid().".png";
            $result = file_put_contents(storage_path('app/avatars/photos/').$namafoto, $binary_data );
           // $data['snap'] = $namafoto;
                
                   $marks = [
                'rand_id' => 'ok',
                'service_id' => $finger_print_service->rand_id,
                'name' => $snap_name,
                'image' =>$namafoto,
            
            ];
                FingerPrintImage::create($marks);
                
            }
    
           
        }
       for($i = 0; $i < count($_POST['house_no']); $i++){
            $house_no =  $_POST['house_no'][$i];
            $apartment_no =  $_POST['apartment_no'][$i];
            $address =  $_POST['address'][$i];
            $city =  $_POST['city'][$i];
            $postal_code =  $_POST['postal_code'][$i];
            $province =  $_POST['province'][$i];
            $country =  $_POST['country'][$i];

            $marks = [
                'rand_id' => $house_no,
                'service_id' => $finger_print_service->rand_id,
                'house_no' => $house_no,
                'apartment_no' =>$apartment_no,
                'address' => $address,
                'city' =>$city,
                'postal_code' =>$postal_code,
                'province' =>$province,
                'country' =>$country,
            ];
            FringerPrintAddress::create($marks);
        }
                if($request->criminal_form_check == "on"){
                    
                          
                $this->validate($request, [
                    'last_name_decalare' => 'required',
                    'given_name_decalare' => 'required',
                    'offence_decalare' => 'required',
                    'date_sentence_decalare' => 'required',
                    'location_of_sentence' => 'required',
                    'date_of_birth_decalare' => 'required',
                    'date_of_sign_decalare' => 'required',
                    'sign_of_applicant_decalare' => 'required',
                    'current_address'=> 'required',
                    'current_city'=> 'required',
                    'current_province'=> 'required',
                    'current_postal_code'=> 'required',
                    'sex' => 'required'
        
                ]);
                    
                            $this->validate($request, [
                    'last_name_verify' => 'required',
                    'given_name_verify' => 'required',
                    'offence_verify' => 'required',
                    'date_sentence_verify' => 'required',
                    'court_location_verify' => 'required',
                    'date_of_birth_verify' => 'required',
                    'date_of_sign_verify'=>'required',
                    'sign_of_applicant_verify' => 'required',
                    'verify_by' => 'required',
                    'sign_of_police' => 'required',
                ]);

        
         $data = $request->all();
        $rand_id = uniqid();
     
     // $data['employee_id'] = Sentinel::getUser()->id;
        $criminal_record_verify = new CriminalRecordVerify;
        $criminal_record_verify->rand_id = $rand_id;
        $criminal_record_verify->last_name = $request->last_name_verify;
            $criminal_record_verify->given_name = $request->given_name_verify;
            $criminal_record_verify->date_of_birth = $request->date_of_birth_verify;
            $criminal_record_verify->date_of_sign = $request->date_of_sign_verify;
        
            $criminal_record_verify->sign_of_applicant = $request->sign_of_applicant_verify;
            $criminal_record_verify->verify_by = $request->verify_by;
            $criminal_record_verify->sign_of_police = $request->sign_of_police;
               $criminal_record_verify->finger_print_id = $finger_print_service->rand_id;
            $criminal_record_verify->save();
            
     //  $finger_print_service = CriminalRecordVerify::create($data);


        for($i = 0; $i < count($_POST['offence_verify']); $i++){
            if(!empty($_POST['offence_verify'][$i]) && !empty($_POST['court_location_verify'][$i]) && !empty($_POST['date_sentence_verify'][$i])){
                $offence =  $_POST['offence_verify'][$i];
                $date_sentence =  $_POST['date_sentence_verify'][$i];
                $location_of_sentence =  $_POST['court_location_verify'][$i];
                $marks = [
                    'rand_id' => 'ok',
                    'criminal_record_id' =>$criminal_record_verify->id,
                    'offence' => $offence,
                    'date_sentence' => $date_sentence,
                    'court_location' =>$location_of_sentence,
                ];
                CriminalRecordVerifyOffence::create($marks);

            }



        }
        
        $rand_id = uniqid();
        $data['rand_id'] = $rand_id;
        // $data['employee_id'] = Sentinel::getUser()->id;
        $delcare_criminal_record = new DelcareCriminalRecord;
        $delcare_criminal_record->rand_id = $rand_id;
        $delcare_criminal_record->last_name = $request->last_name_decalare;
            $delcare_criminal_record->given_name = $request->given_name_decalare;
        $delcare_criminal_record->sex = $request->sex;
           $delcare_criminal_record->current_address = $request->current_address;
           $delcare_criminal_record->city = $request->current_city;
        $delcare_criminal_record->province = $request->current_province;
        $delcare_criminal_record->postal_code = $request->current_postal_code;
            $delcare_criminal_record->date_of_birth = $request->date_of_birth_decalare;
            $delcare_criminal_record->date_of_sign = $request->date_of_sign_decalare;
        
            $delcare_criminal_record->sign_of_applicant = $request->sign_of_applicant_decalare;
            $delcare_criminal_record->police_info_attach = $request->police_info_attach;
        $delcare_criminal_record->finger_print_id = $finger_print_service->rand_id;
            $delcare_criminal_record->save();
        
        //$finger_print_service = DelcareCriminalRecord::create($data);


        for($i = 0; $i < count($_POST['offence_decalare']); $i++){
            if(!empty($_POST['offence_decalare'][$i]) && !empty($_POST['date_sentence_decalare'][$i]) && !empty($_POST['location_of_sentence'][$i])){
                $offence =  $_POST['offence_decalare'][$i];
                $date_sentence =  $_POST['date_sentence_decalare'][$i];
                $location_of_sentence =  $_POST['location_of_sentence'][$i];
                $marks = [
                    'rand_id' => 'ok',
                    'declare_record_id' =>$delcare_criminal_record->id,
                    'offence' => $offence,
                    'date_sentence' => $date_sentence,
                    'location_of_sentence' =>$location_of_sentence,
                ];
                DelcareCriminalRecordOffence::create($marks);

            }



        }
          }
        
       
        $case = new Cases;
        $case->rand_id = 'ok';
        $case->office_id = $rand_id;
        //$case->closed_date =  date('Y-m-d');
        $case->service_id = $rand_id;
        $case->service_name = 'Finger Prints Form';
        $case->police_id = $finger_print_service->id;
         $case->case_owner =Sentinel::getUser()->id;
        $case->status = 1;
        $case->save();
        $msg = "Case  is created";
        Session::flash('success', $msg);
        return redirect('/customer-cases');
    }
     public function updateCase(Request $request,$id){
        
        $service_id = Cases::where('id',$id)->first();
       
         $this->validate($request, [
            'last_name' => 'required',
            'given_name' => 'required',
            'last_name_birth' => 'required',
            'former_name' => 'required',
            'birth_place' => 'required',
            'date_of_birth' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'house_no' => 'required',
            'apartment_no' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'province' => 'required',
            'country' => 'required',
            'reason_to_request' => 'required',
            'contact_name' => 'required',
            'organization_request' => 'required',
            'organization_address' => 'required',
            'contact_no' => 'required',
            'auth_to' => 'required',
            'auth_located' => 'required',
            'auth_location' => 'required',
            // 'auth_signature' => 'required',
            'date_of_sign' => 'required',
            'auth_city' => 'required',
            'auth_province' => 'required',
            'auth_witness_agent' => 'required',
            'auth_identification' => 'required',
            'auth_id' => 'required',



        ]);

        if(Input::file('passport')){
              $image = Input::file('passport');
        $passport  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('/application-file/img/' . $passport);
        Image::make($image->getRealPath())->save($path);
        // $data['passport'] = $passport;
            
        //     $passport = $request->file('passport')->store('avatars');
        //     $passport = Storage::putFile('avatars', $request->file('passport'));
        
    FingerPrint::where('rand_id', $service_id->service_id)->update([
            'passport' => $passport         
          ]);
        }
        
       
        if(Input::file('idcard')){
     $image = Input::file('idcard');
        $idcard  = time() . '.' . $image->getClientOriginalExtension();
         $path = public_path('/application-file/img/' . $idcard);
        Image::make($image->getRealPath())->save($path);
            // $idcard = $request->file('idcard')->store('avatars');
            // $idcard = Storage::putFile('avatars', $request->file('idcard'));
        
     FingerPrint::where('rand_id', $service_id->service_id)->update([
            'idcard' => $idcard         
          ]);
            
        }

        FingerPrint::where('rand_id', $service_id->service_id)->update([
            'last_name' => $request->last_name,
            'given_name' => $request->given_name,
            'last_name_birth' =>$request->last_name_birth,
            'former_name' =>$request->former_name,
            'birth_place' => $request->birth_place,
            'date_of_birth' => $request->date_of_birth,
            'phone_number' => $request->phone_number,
            'gender' => $request->gender,
            'reason_to_request' => $request->reason_to_request,
            'contact_name' => $request->contact_name,
            'organization_request' =>$request->organization_request,
            'organization_address' => $request->organization_address,
            'contact_no' => $request->contact_no,
            'auth_to' => $request->auth_to,
            'auth_located' => $request->auth_located,
              'cpic_investegation'=> $request->cpic_investegation,
              'police_info'=> $request->police_info,
              'other'=> $request->other,
            'auth_location' => $request->auth_location,
            'date_of_sign' => $request->date_of_sign,
            'auth_city' => $request->auth_city,
            'auth_province' => $request->auth_province,
            'auth_witness_agent' => $request->auth_witness_agent,
            'auth_identification' => $request->auth_identification,
            'auth_witness_agent_signature' => $request->auth_witness_agent_signature,
            'auth_id' => $request->auth_id,
          ]);
        
        
        
        if(isset($_POST['house_no'])){
            $existence_address = FringerPrintAddress::where('service_id',$service_id->service_id)->get();
            
            if(count($existence_address)>0){
         FringerPrintAddress::where('service_id',$service_id->service_id)->delete();
        for($i = 0; $i < count($_POST['house_no']); $i++){
            $house_no =  $_POST['house_no'][$i];
            $apartment_no =  $_POST['apartment_no'][$i];
            $address =  $_POST['address'][$i];
            $city =  $_POST['city'][$i];
            $postal_code =  $_POST['postal_code'][$i];
            $province =  $_POST['province'][$i];
            $country =  $_POST['country'][$i];

            $marks = [
                'rand_id' => $house_no,
                'service_id' => $service_id->service_id,
                'house_no' => $house_no,
                'apartment_no' =>$apartment_no,
                'address' => $address,
                'city' =>$city,
                'postal_code' =>$postal_code,
                'province' =>$province,
                'country' =>$country,
            ];
            FringerPrintAddress::create($marks);
        }
            }
         
        }
          
        if(!empty($_POST['namafoto'])){
            $image= explode(",",$request->namafoto);
              $name= explode(",",$request->image_name);
     
            
            for($i = 0; $i < count($image); $i++){
            
                $snap_image =  $image[$i];
                $snap_name =  $name[$i];
                
            $binary_data = base64_decode($snap_image);
            $namafoto = uniqid().".png";
            $result = file_put_contents(storage_path('app/avatars/photos/').$namafoto, $binary_data );
           // $data['snap'] = $namafoto;
                
                   $marks = [
                'rand_id' => 'ok',
                'service_id' => $service_id->service_id,
                'name' => $snap_name,
                'image' =>$namafoto,
            
            ];
                FingerPrintImage::create($marks);
                
            }
    
           
        }
        
        CriminalRecordVerify::where('finger_print_id', $service_id->service_id)->update([
            'last_name'=> $request->last_name_verify,
            'given_name'=> $request->given_name_verify,
            'date_of_birth'=> $request->date_of_birth_verify,
            'date_of_sign'=> $request->date_of_sign_verify,
            'sign_of_applicant'=> $request->sign_of_applicant_verify,
            'verify_by'=> $request->verify_by,
            'sign_of_police'=> $request->sign_of_police,
          
        ]);
        if(isset($_POST['offence_verify'])){
             $existence  = CriminalRecordVerifyOffence::where('criminal_record_id',$service_id->service_id)->get();
           if(count($existence) > 0){
               
               CriminalRecordVerifyOffence::where('criminal_record_id',$service_id->service_id)->delete();
                   for($i = 0; $i < count($_POST['offence_verify']); $i++){
          
            if(!empty($_POST['offence_verify'][$i]) && !empty($_POST['court_location_verify'][$i]) && !empty($_POST['date_sentence_verify'][$i])){
                $offence =  $_POST['offence_verify'][$i];
                $date_sentence =  $_POST['date_sentence_verify'][$i];
                $location_of_sentence =  $_POST['court_location_verify'][$i];
                $marks = [
                    'rand_id' => 'ok',
                    'criminal_record_id' =>$criminal_record_verify->id,
                    'offence' => $offence,
                    'date_sentence' => $date_sentence,
                    'court_location' =>$location_of_sentence,
                ];
                CriminalRecordVerifyOffence::create($marks);

            }



        }
           }
        
        }
        // DelcareCriminalRecord::where('finger_print_id', $service_id->service_id)->update([
        //     'last_name' => $request->last_name_decalare,
        //     'given_name' => $request->given_name_decalare,
        //     'sex' => $request->sex,
        //     'current_address' => $request->current_address,
        //     'city' => $request->city,
        //     'province' => $request->province,
        //     'postal_code' => $request->postal_code,
        //     'date_of_birth' => $request->date_of_birth_decalare,
        //     'date_of_sign' => $request->date_of_sign_decalare,
        //     'sign_of_applicant' => $request->sign_of_applicant_decalare,
        //     'sign_of_applicant' => $request->sign_of_applicant_decalare,
        
        // ]);
        
        //               if(isset($_POST['offence_verify'])){
        //      $existence  = DelcareCriminalRecordOffence::where('criminal_record_id',$service_id->service_id)->get();
        //   if(count($existence) > 0){
               
        //       DelcareCriminalRecordOffence::where('criminal_record_id',$service_id->service_id)->delete();

        // for($i = 0; $i < count($_POST['offence_decalare']); $i++){
        //     if(!empty($_POST['offence_decalare'][$i]) && !empty($_POST['date_sentence_decalare'][$i]) && !empty($_POST['location_of_sentence'][$i])){
        //         $offence =  $_POST['offence_decalare'][$i];
        //         $date_sentence =  $_POST['date_sentence_decalare'][$i];
        //         $location_of_sentence =  $_POST['location_of_sentence'][$i];
        //         $marks = [
        //             'rand_id' => 'ok',
        //             'declare_record_id' =>$delcare_criminal_record->id,
        //             'offence' => $offence,
        //             'date_sentence' => $date_sentence,
        //             'location_of_sentence' =>$location_of_sentence,
        //         ];
        //         DelcareCriminalRecordOffence::create($marks);

        //     }
        // }
        //   }


        // }
             $msg = "Case  is Updated";
        Session::flash('success', $msg);
         return redirect()->back();
       // return redirect('/customer-cases/'.$id);

    }
    public function editCase($id){
        
        $case = Cases::select('cases.id','finger_print.id as finger_id','finger_print.rand_id as finger_rand_id','declare_criminal_record.id as declare_id','criminal_record_verify.id as verify_id','finger_print.last_name as finger_last_name','criminal_record_verify.last_name as verify_last_name','declare_criminal_record.last_name as declare_last_name','finger_print.given_name as finger_given_name','criminal_record_verify.given_name as verify_given_name','declare_criminal_record.given_name as declare_given_name','last_name_birth','former_name','birth_place','finger_print.date_of_birth as finger_date_of_birth','criminal_record_verify.date_of_birth as verify_date_of_birth','declare_criminal_record.date_of_birth as declare_date_of_birth','phone_number','contact_no','gender','reason_to_request','contact_name','organization_request','organization_address','cpic_investegation','police_info','other','auth_to','auth_located','auth_location','auth_signature','finger_print.date_of_sign as finger_date_of_sign','criminal_record_verify.date_of_sign as verify_date_of_sign','declare_criminal_record.date_of_sign as declare_date_of_sign','auth_city','auth_province','auth_witness_agent','auth_identification','auth_witness_agent_signature','auth_id','passport','idcard','sex','current_address','city','province','postal_code','criminal_record_verify.sign_of_applicant as verify_sign_of_applicant','declare_criminal_record.sign_of_applicant as declare_sign_of_applicant','verify_by','sign_of_police')
                ->join("finger_print","finger_print.rand_id", "=", "cases.service_id")
                ->leftJoin("criminal_record_verify","criminal_record_verify.finger_print_id", "=", "finger_print.id")
                ->leftJoin("declare_criminal_record","declare_criminal_record.finger_print_id", "=", "finger_print.id")
                    ->where('cases.id',$id)->first(); 
//                    if($cases->status == 1 ){
//                      
//                          Cases::where('id', $id)->update(['status'=> 2,'open_date' =>date('Y-m-d')]);
//                    }
        
       
   
            return view('customer.forms.edit-fingerprint',compact('case'));
    } 
    
    public function imageAttachment($id){
                $card_images = FingerPrint::select('passport','idcard')->where('rand_id',$id)->first();
        $images = FingerPrintImage::where('service_id',$id)->get();
         return view('customer.attachment',compact('images','card_images'));
    }
    public function index()
    {
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
