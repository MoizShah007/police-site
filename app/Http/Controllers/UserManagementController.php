<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\User;
use App\Offices;
use App\Customer;
use App\CustomerOffices;
use App\Police;
use App\PoliceService;
use App\PoliceOffice;
use App\Services;
use App\EmployeeOffices;
use Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Database\Eloquent\Model;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Session;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $employee = Employee::join("users", "users.id", "=", "employee_info.user_id")
            ->where('users.status',1)
            ->get();
        $customer = Customer::join("users", "users.id", "=", "customer_info.user_id")
            ->where('users.status',1)
            ->get();
        $police = Police::join("users", "users.id", "=", "police_info.user_id")
            ->where('users.status',1)->get();
         
        return view('admin.user-management.index',compact('employee','police','customer'));
    }

    public function policeList(){
        $employee = Employee::join("users", "users.id", "=", "employee_info.user_id")
            ->where('users.status',1)
            ->get();
             $customer = Customer::join("users", "users.id", "=", "customer_info.user_id")
            ->where('users.status',1)
            ->get();
        $police = Police::join("users", "users.id", "=", "police_info.user_id")
            ->where('users.status',1)->get();
        return view('admin.user-management.police',compact('police','employee','customer'));
    }
          public function customerList(){
        $employee = Employee::join("users", "users.id", "=", "employee_info.user_id")
            ->where('users.status',1)
            ->get();
              $customer = Customer::join("users", "users.id", "=", "customer_info.user_id")
            ->where('users.status',1)
            ->get();
        $police = Police::join("users", "users.id", "=", "police_info.user_id")
            ->where('users.status',1)->get();

        return view('admin.user-management.customer',compact('police','employee','customer'));
    }
    public function policeProfilelist(){
        $police = Police::join("users", "users.id", "=", "police_info.user_id")
            ->where('users.status',1)->get();
        return view('admin.police.index',compact('police'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $office = Offices::where('status',1)->get();
        return view('admin.user-management.create-employee',compact('office'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            //            'current-password' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',

        ]);
        $data =  $request->all();
        $user = Sentinel::create(
            [
                'email' => $data['email'],
                'password' => $data['password'],
                'first_name' =>$data['first_name'],
                'last_name' =>$data['last_name'],
                'status' => 1,
            ]
        );

        // Activate the admin directly
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);

        // Find the group using the group id
        $adminGroup = Sentinel::findRoleBySlug('employee');

        // Assign the group to the user
        $adminGroup->users()->attach($user);

        $image = $request->file('image');
        if(count($image) > 0){
            $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('application-file/img/' . $filename);
        Image::make($image->getRealPath())->resize(200, 300)->save($path);
        $data['image'] = $filename;
        }
        $data['rand_id'] = 'ok';
        $data['office_id'] = $request->office_id[0];
        $data['user_id'] = $user->id;
        Employee::create($data);
        foreach($request->office_id as $value){
             $employeeOffices = new EmployeeOffices();
            $employeeOffices->rand_id = 'ok';
                $employeeOffices->office_id = $value;
                $employeeOffices->employee_id = $user->id;
                $employeeOffices->save();
        }
       
          $msg = "Employee  is added";
               Session::flash('success', $msg);
               return redirect('/user-management');

    }

    public function policeCreate(){
        $service = Services::where('status',1)->get();
        $office = Offices::where('status',1)->get();
        return view('admin.user-management.create-police',compact('office','service'));
    }
    public function createPolice(Request $request){

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'location' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',

        ]);
        $data =  $request->all();
        $user = Sentinel::create(
            [
                'email' => $data['email'],
                'password' => $data['password'],
                'first_name' =>$data['first_name'],
                'last_name' =>$data['first_name'],
                'status' => 1,
            ]
        );
        // Activate the admin directly
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);

        // Find the group using the group id
        $adminGroup = Sentinel::findRoleBySlug('police');

        // Assign the group to the user
        $adminGroup->users()->attach($user);

        $image = $request->file('image');
     
        if(count($image)>0){
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('application-file/img/' . $filename);
        Image::make($image->getRealPath())->resize(200, 300)->save($path);
        $data['image'] = $filename;
        }
        $data['rand_id'] = 'ok';
        $data['user_id'] = $user->id;
        $data['office_id'] = $user->id;
        $police = Police::create($data);
//        foreach($request->service_id as $value){
//            $police_service = new PoliceService;
//            $police_service->service_id = $value;
//            $police_service->rand_id='ok';
//            $police_service->police_depart_id= $police->id;
//            $police_service->save();
//        }
//        foreach($request->office_id as $value){
//            $police_office = new PoliceOffice;
//            $police_office->office_id = $value;
//            $police_office->rand_id='ok';
//            $police_office->police_depart_id= $police->id;
//            $police_office->save();
//        }


      $msg = "Police  is added";
               Session::flash('success', $msg);
          return redirect('/user-management'); 
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::join("users", "users.id", "=", "employee_info.user_id")
            ->where('users.id',$id)
            ->first();
        $office = Offices::where('status',1)->get();
        return view('admin.user-management.edit-employee',compact('office','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
      public function update(Request $request, $id)
    {

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
       
        ]);
        $data =  $request->all();
        //      echo '<pre>';
        //        print_r($data);
        //        exit;
        $user = Sentinel::findById($id);

        if($request->has('password') && $request->has('password_confirmation')){
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
            ]);
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'password' => $data['password'],
            ];
        }else{
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],

            ];
        }
        
        $data = request()->except(['_token','_method','first_name','last_name','password','password_confirmation']);

        if($request->file('image')){
            $image = $request->file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('application-file/img/' . $filename);
            Image::make($image->getRealPath())->resize(200, 300)->save($path);
           
        Employee::where('user_id', $id)->update(['location'=>$data['location'],'phone'=>$data['phone'], 'address'=>$data['address'],'city'=>$data['city'],'country'=>$data['country'],'street'=>$data['street'],'state'=>$data['state'],'zip'=>$data['zip'],'image'=>$filename]);

        }

        $user = Sentinel::update($user, $credentials);
        
   
        Employee::where('user_id', $id)->update(['location'=>$data['location'],'phone'=>$data['phone'], 'address'=>$data['address'],'city'=>$data['city'],'country'=>$data['country'],'street'=>$data['street'],'state'=>$data['state'],'zip'=>$data['zip']]);
        
            if(count($request->office_id)>0){
                EmployeeOffices::where('employee_id',$id)->delete();
                foreach($request->office_id as $value){

                    $office_service = new EmployeeOffices;
                    $office_service->employee_id =  $id;
                    $office_service->rand_id='ok';
                    $office_service->office_id= $value;
                    $office_service->save();
                }
            }
        $msg = "Employee  is updated";
               Session::flash('success', $msg);
               return redirect('/user-management'); 
        //       $msg = "Employee  is updated";
        //       Session::flash('success', $msg);
        //      return redirect('user-/');
    }
    // public function update(Request $request, $id)
    // {

    //     $this->validate($request, [
    //         'first_name' => 'required|string|max:255',
    //         'last_name' => 'required|string|max:255',
    //         'location' => 'required',
    //         'phone' => 'required|numeric',
    //         'address' => 'required',
    //         'street' => 'required',
    //         'city' => 'required',
    //         'state' => 'required',
    //         'country' => 'required',
    //         'zip' => 'required|numeric',

    //     ]);
    //     $data =  $request->all();
    //     //      echo '<pre>';
    //     //        print_r($data);
    //     //        exit;
    //     $user = Sentinel::findById($id);

    //     if($request->has('password') && $request->has('password_confirmation')){
    //         $this->validate($request, [
    //             'password' => 'required|min:6|confirmed',
    //             'password_confirmation' => 'required|min:6',
    //         ]);
    //         $credentials = [
    //             'first_name' => $data['first_name'],
    //             'last_name' => $data['last_name'],
    //             'password' => $data['password'],
    //         ];
    //     }else{
    //         $credentials = [
    //             'first_name' => $data['first_name'],
    //             'last_name' => $data['last_name'],

    //         ];
    //     }
        
    //     $data = request()->except(['_token','_method','first_name','last_name','password','password_confirmation']);



    //     if($request->file('image')){
    //         $image = $request->file('image');
    //         $filename  = time() . '.' . $image->getClientOriginalExtension();
    //     $path = public_path('application-file/img/' . $filename);
    //         Image::make($image->getRealPath())->resize(200, 300)->save($path);
    //         $data['image'] = $filename;

    //     }




    //     $user = Sentinel::update($user, $credentials);
    //     Employee::where('user_id', $id)->update($data);
    //     $msg = "Employee  is updated";
    //           Session::flash('success', $msg);
    //           return redirect('/user-management'); 
    //     //       $msg = "Employee  is updated";
    //     //       Session::flash('success', $msg);
    //     //      return redirect('user-/');
    // }

    public function editPolice($id){
        $police = Police::join("users", "users.id", "=", "police_info.user_id")
            ->where('users.id',$id)->first();
        $service = Services::where('status',1)->get();
        $office = Offices::where('status',1)->get();
        return view('admin.user-management.edit-police',compact('police','office','service'));
    }

    public function updatePolice(Request $request, $id){

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'location' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',

        ]);
        $data =  $request->all();
        //      echo '<pre>';
        //        print_r($data);
        //        exit;
        $user = Sentinel::findById($id);

        if($request->has('password') && $request->has('password_confirmation')){
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
            ]);
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['first_name'],
                'password' => $data['password'],
            ];
        }else{
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['first_name'],

            ];
        }

        $user = Sentinel::update($user, $credentials);

        if($request->file('image')){
            $image = $request->file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
           $path = public_path('application-file/img/' . $filename);
            Image::make($image->getRealPath())->resize(200, 300)->save($path);
            $data['image'] = $path;

            $police = Police::where('user_id', $id)->update(['location'=>$request->location,'phone'=>$request->phone, 'address'=> $request->address,'city'=> $request->city,'country'=> $request->country,'street'=>$request->street,'state'=> $request->state,'zip'=> $request->zip,'office_id'=>0,'image'=> $filename]);

        }else{
            $police =  Police::where('user_id', $id)->update(['location'=> $request->location, 'phone'=> $request->phone, 'address'=> $request->address,'city'=> $request->city,'country'=> $request->country,'street'=> $request->street,'state'=> $request->state,'zip'=> $request->zip,'office_id'=>0]);
        }

//        if($request->has('office_id')){
//            $police_id = Police::where('user_id', $id)->first();
//            foreach($request->office_id as $value){
//                $police_office = new PoliceOffice;
//                $police_office->office_id = $value;
//                $police_office->rand_id='ok';
//                $police_office->police_depart_id= $police_id->id;
//                $police_office->save();
//            }
//        }
//        if($request->has('service_id')){
//            foreach($request->service_id as $value){
//                $police_service = new PoliceService;
//                $police_service->service_id = $value;
//                $police_service->rand_id='ok';
//                $police_service->police_depart_id= $police_id->id;
//                $police_service->save();
//            }
//        }
       $msg = "Police  is updated";
               Session::flash('success', $msg);
               return redirect('/user-management'); 
    }
     public function createCustomer()
    {
          return view('admin.user-management.create-customer');
      //  return view('admin.user-management.create-customer',compact('office'));
    }

    public function customerCreate(Request $request){
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            //            'current-password' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',

        ]);
        $data =  $request->all();
        $user = Sentinel::create(
            [
                'email' => $data['email'],
                'password' => $data['password'],
                'first_name' =>$data['first_name'],
                'last_name' =>$data['last_name'],
                'status' => 1,
            ]
        );
        // Activate the admin directly
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);

        // Find the group using the group id
        $adminGroup = Sentinel::findRoleBySlug('user');

        // Assign the group to the user
        $adminGroup->users()->attach($user);

        $image = $request->file('image');
        if(count($image) > 0){
            $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('application-file/img/' . $filename);
        Image::make($image->getRealPath())->resize(200, 300)->save($path);
        $data['image'] = $filename;
        }
        $data['rand_id'] = 'ok';
        $data['office_id'] = $request->office_id[0];
        $data['user_id'] = $user->id;
        Customer::create($data);
        // foreach($request->office_id as $value){
        //      $customerOffices = new CustomerOffices();
        //     $customerOffices->rand_id = 'ok';
        //         $customerOffices->office_id = $value;
        //         $customerOffices->customer_id = $user->id;
        //         $customerOffices->save();
        // }
       
          $msg = "Customer  is added";
               Session::flash('success', $msg);
               return redirect('/customer-lists');
    }
    
         public function editCustomer($id)
    {
        $customer = Customer::join("users", "users.id", "=", "customer_info.user_id")
            ->where('users.id',$id)
            ->first();
       // $office = Offices::where('status',1)->get();
         return view('admin.user-management.edit-customer',compact('customer'));
        // return view('admin.user-management.edit-customer',compact('office','customer'));
    }
     public function updateCustomer(Request $request, $id){
     
         $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
       
        ]);
        $data =  $request->all();
        //      echo '<pre>';
        //        print_r($data);
        //        exit;
        $user = Sentinel::findById($id);

        if($request->has('password') && $request->has('password_confirmation')){
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
            ]);
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'password' => $data['password'],
            ];
        }else{
            $credentials = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],

            ];
        }
        
        $data = request()->except(['_token','_method','first_name','last_name','password','password_confirmation']);

        if($request->file('image')){
            $image = $request->file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('application-file/img/' . $filename);
            Image::make($image->getRealPath())->resize(200, 300)->save($path);
           
        Customer::where('user_id', $id)->update(['location'=>$data['location'],'phone'=>$data['phone'], 'address'=>$data['address'],'city'=>$data['city'],'country'=>$data['country'],'street'=>$data['street'],'state'=>$data['state'],'zip'=>$data['zip'],'image'=>$filename]);

        }

        $user = Sentinel::update($user, $credentials);
        
   
        Customer::where('user_id', $id)->update(['location'=>$data['location'],'phone'=>$data['phone'], 'address'=>$data['address'],'city'=>$data['city'],'country'=>$data['country'],'street'=>$data['street'],'state'=>$data['state'],'zip'=>$data['zip']]);
       
       
    //         if(count($request->office_id)>0){
    //             CustomerOffices::where('customer_id',$id)->delete();
    //   foreach($request->office_id as $value){
    //          $customerOffices = new CustomerOffices();
    //         $customerOffices->rand_id = 'ok';
    //             $customerOffices->office_id = $value;
    //             $customerOffices->customer_id = $user->id;
    //             $customerOffices->save();
    //     }
    //         }
        $msg = "Customer  is updated";
               Session::flash('success', $msg);
               return redirect('/customer-lists'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function policeDelete(Request $request){
      
        $request_data = $request->favorite;
        foreach ($request_data as $key ) {
             User::where('id', $key)->update(['status' => 0]);
        }
       $msg = "Police is deleted";
       Session::flash('success', $msg);
       return redirect('service/');

    }
 public function deletePolice($id){
        User::where('id', $id)->update(['status' => 0]);
               $msg = "Police  is deleted";
               Session::flash('success', $msg);
               return redirect()->back();

    }
    public function deleteEmployee($id){
        User::where('id', $id)->update(['status' => 0]);
        
               $msg = "Employee  is deleted";
               Session::flash('success', $msg);
               return redirect()->back();

    }
        public function deleteCustomer($id){
        User::where('id', $id)->update(['status' => 0]);
        
               $msg = "Customer is deleted";
               Session::flash('success', $msg);
               return redirect()->back();

    }
    public function destroy($id)
    {
        //
    }
}
